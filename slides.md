## Git patterns and best practices

Alvin B // Prodyna London // 2019-11-19



## Summary
1. Very brief intro to VCS
2. Git best practices
3. Git patterns
4. Gits bits

Note: 
- 1 = Primer for non techs



![](images/the-real-vcs-sm.jpg)
<!-- <img src="images/the-real-vcs.jpg" height="100%"> -->

Note:
- Without VCS: overwritten & deleted code, no versioning, no rollbacks
- FTPing in to server
- at best(!) versioned folder numbers!
- Solution: VCS
- What is VCS? Logs every change. Can look at who edited what, when and hopefully why
- Why good? Collabration, storing versions, rollback, understanding history, backup (side effect)


## History

![](images/vcs-history.png)

Note: 
- CVS break? when pushing changes, had to coordinate pushs
- so thankful they exist, but need consistant workflow, workflows improved over time (this presentation)



## 2 - Git best practices


Prerequisite: good project management, communication and team work

Note:
- We need small, manageable tasks. Try to ensure people aren't working on same thing if possible


One complete task = one commit 

Note:
- Not: > 1 to 1, to 1 to > 1
- Ensures frequent commits. "Commit often" ethos
- Don't commit half done work (git isn't your backup service)


Good & consistent **commit messages**

[Project conventions example](http://karma-runner.github.io/4.0/dev/git-commit-msg.html), [good commit example](https://fatbusinessman.com/2019/my-favourite-git-commit)

![](images/xkcd-git.png)

Note:
- Short summary (50 - 70 characters), new line, body
- Commit type, scope. Ticket number (footer?)
- What has changed? See diff
- More importantly why has it changed, why did you do it like that?
- Link to the commit from group chat/wiki
- Change log is self creating
- Even if time to create message is >> time to make change


Rebasing? 

Full branch squash?!

Note:
- Readability vs accuracy
- Rebasing could be dangerous if you get it wrong


Git is **not** a backup service

Note:
- if you think of it as so then you won't commit semantically
- there will be half done work etc


Test before you commit!


Choose a convention/**your** best practices and stick to them

Note:
- be consistent


But how and when should I branch?!



## 3 - Git patterns

Note:
- aka git workflows, or git branching patterns


### Git Flow
<!-- ![](images/git-flow.png) -->
<img src="images/git-flow.png" width="50%">

Note:
- 2010, first wildly adopted pattern - consistency
- Conways law: "organizations which design systems ... are constrained to produce designs which are copies of the communication structures of these organizations". Similar thing happens in git repo
- creates long lived branches. A branch is debt that has to be paid off
- leads to "merge hell", merge in to master, back in to feature branch
- leads to massive, unprocessable pull requests
- not optimum for CI/CD


### Aside: CI/CD

<img src="images/cicd.jpg" width="100%">

Note:
- many deploys per day. Full auto testing. 
- CD: lower risk deploys, faster time to market, agility: increased quality and happiness!


### Truck based development

<img src="images/tbd.png" width="100%">

Note:
- Short lived branches, 1 person, 1-2 days
- Fewer merge conflicts
- Far easier code reviews
- C delivery vs deployment- netflix, etsy etc - no releases
- But big problem: unfinished features. Answer: feature flags


### Feature flags

<img src="images/feature-flag.png" width="100%">

Note:
- Simple conditionals in the code
- FFaas allows incrementally rolled out feature


### Github flow

<img src="images/github-flow.png" width="100%">

Note:
- type of trunk based development
- Good for continuous deployment (c.f. delivery)
- Deploy before merge back in to master
- final thought: no pattern suitable for all situations



## 4 - Gits bits
- Git submodules are evil
- [Automated semantic versioning](https://github.com/semantic-release/semantic-release)
- `git lg`
- `git bisect`
- any others?


<img src="images/git-log.png" width="100%">


<img src="images/git-lg.png" width="100%">



EoL